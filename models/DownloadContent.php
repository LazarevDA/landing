<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 04.09.2017
 * Time: 15:43
 */

namespace app\models;



class DownloadContent extends Main
{
    public $userURL;
    public $userAgent;
    public $ftpDir;
    public $errorsMessages = [];
    public $content;
    public $js = [];
    public $css = [];
    public $img = [];
    public $elementNumber;
    /**
     * template config:
     * 'download' => [
     *     'css' => [
     *         [
     *             'stringSearch' => [
     *                 'start' => '<link',
     *                 'end' => '>',
     *                 'attr' => [
     *                     'attrName' => 'rel',
     *                     'attrValue' => 'stylesheet',
     *                 ],
     *             ],
     *             'patternSearch' => "/\<[Ll][Ii][Nn][Kk][^\>]*[Rr][Ee][Ll]\=(\"|')([Ss][Tt][Yy][Ll][Ee][Ss][Hh][Ee][Ee][Tt])(\"|')[^\>]*>/",
     *             'attr' => 'href',
     *             'recursionContent' => [
     *                 'img' => [
     *                     'patternSearch' => "/(url)\([^\)]*\.([Pp][Nn][Gg]|[Jj][Pp][Gg]|[Ss][Vv][Gg]|[Jj][Pp][Ee][Gg])[^\)]*\)/",
     *                     'path' => 'img',
     *                     'url' => '../img',
     *                 ],
     *                 'font' => [
     *                     'patternSearch' => "/(url)\([^\)]*\.([Tt][Ii][Ff][Ff]|[Ee][Oo][Tt]|[Ww][Oo][Ff][Ff]|[Tt][Tt][Ff])[^\)]*\)/",
     *                     'path' => 'css/font',
     *                     'url' => 'font',
     *                 ],
     *                 'css' => [
     *                     'patternSearch' => "/(url)\([^\)]*\.([Cc][Ss][Ss])[^\)]*\)/",
     *                     'path' => 'css',
     *                     'url' => '../css',
     *                     'recursion' => true
     *                 ],
     *             ]
     *         ]
     *     ],
     *     'img' => [
     *         [
     *             'patternSearch' => '/\<[Aa][^\>]*[Hh][Rr][Ee][Ff]\=("|\')[^"|\']*.([Pp][Nn][Gg]|[Jj][Pp][Gg]|[Ss][Vv][Gg]|[Jj][Pp][Ee][Gg])("|\')[^\>]*\>/',
     *             'attr' => 'href',
     *         ],
     *         [
     *             'patternSearch' => '/(url)\([^\)]*\.([Pp][Nn][Gg]|[Jj][Pp][Gg]|[Ss][Vv][Gg]|[Jj][Pp][Ee][Gg])[^\)]*\)/',
     *             'patternAttr' => [
     *                 'pattern' => '/(\(["\']?)([^\)]*)(["\']?\))/',
     *                 'matchNumber' => 2,
     *             ]
     *         ],
     *         [
     *             'stringSearch' => [
     *                 'start' => '<img',
     *                 'end' => '>',
     *             ],
     *             'patternSearch' => '/(\<[Ii][Mm][Gg])[^\>]*\>/',
     *             'attr' => 'src',
     *         ],
     *     ],
     *     'js' => [
     *         [
     *             'stringSearch' => [
     *                 'startWithoutTag' => '>',
     *                 'start' => '<script',
     *                 'end' => '</script>',
     *             ],
     *             'attr' => 'src',
     *             'template' => '<script src="{attr}" type="text/javascript"></script>',
     *             'comment' => true,
     *             'addUserContent' => [
     *                 'ftp' => [
     *                     'template' => '<script src="http:/{attr}" type="text/javascript"></script>',
     *                     'whatAdd' => [
     *                         '</head>' => FTP_LIBS_JS."/head",
     *                         '</body>' => FTP_LIBS_JS."/body",
     *                     ]
     *                 ]
     *             ]
     *         ]
     *     ],
     * ],
     * 'uploadOnFTP' => true
     */
    public $config;
    public $defaultDir = '../tempFile';
    public $uploadType;
    public $downloadResult;
    public $searchLinks;
    public $links;
    public $addUserContent;
    public $addUserContentTemplate = [
        'favicon' => [
            'remove' => '/\<[Ll][Ii][Nn][Kk][^\>]*[Rr][Ee][Ll]\=[\'|"]?[Ii][Cc][Oo][Nn][\'|"]?[^\>]*\>/',
            'where' => '</head>',
            'template' => '<link href="http:/{attr}" rel="icon" type="image/png"/>'
        ],
        'jsHead' => [
            'where' => '</head>',
            'template' => '<script src="http:/{attr}" type="text/javascript"></script>'
        ],
        'jsBody' => [
            'where' => '</body>',
            'template' => '<script src="http:/{attr}" type="text/javascript"></script>'
        ],
        'php' => [
            'where' => '</body>',
            'template' => '<?php $ch = curl_init("http:/{attr}");curl_exec($ch);curl_close($ch);?>',
        ],
    ];

    public function rules()
    {
        return [
            [['addUserContent', 'links'], 'safe'],
            [['userURL','uploadType',], 'required'],
            [['userURL', 'uploadType', 'ftpDir', 'downloadResult', 'searchLinks','userAgent'], 'string'],
            ['ftpDir', 'required', 'when' => function ($model) {
                return $model->uploadType == 'ftp';
            }, 'whenClient' => "function (attribute, value) {return $('#downloadcontent-uploadtype input[type=radio]:checked').val() == 'ftp';}"
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'userURL' => 'URL',
            'ftpDir' => 'Директория для сохранения на сервере',
            'uploadType' => 'Тип выгрузки',
            'searchLinks' => 'Найти ссылки',
        ];
    }

    /**
     * Добавление пользовательского контента
     */
    public function addUserContent()
    {
        foreach ($this->addUserContent as $name => $element){
            $str = '';
            $template = $this->addUserContentTemplate[$name];
            if(isset($template['remove'])){
                $this->content = preg_replace($template['remove'], '',$this->content);
            }
            foreach ($element as $url){
                if(isset($template['template'])) {
                    $str .= str_replace('{attr}', $url, $template['template']) . "\n";
                }
            }
            $this->content = str_replace($template['where'],"\n".$str . $template['where'], $this->content);
        }
    }

    /**
     * Возвращает URl, указанный пользователем в интерфейсе загрузки, на основе полученного шаблона($pattern).
     * Используется для загрузки подключаемых файлов или библиотек, для которых указан относительны путь
     * @param $pattern - название шаблона, который необходимо получить
     * @param int $up - количество итераций выхода на катлог выше, которые необходимо выполнить, если путь файла ../
     * @return string - Обработанный URL
     */
    public function getBaseUrl($pattern, $up = 0)
    {
        switch ($pattern){
            case 'onlyDomain':
                preg_match('/([Hh][Tt][Tt][Pp][Ss]*:\/\/)*[^\/]*/', $this->userURL, $matches);
                return $matches[0];
            case 'upCurrentUrl':
                $url = preg_replace('/\/[^\/]*\.([Pp][Hh][Pp]|[Hh][Tt][Mm][Ll]|[Cc][Ss][Ss])|(\/*\?.*)/','', $this->userURL);
                while($up != 0 ){
                    $url = substr($url,0, strripos($url, '/'));
                    $up--;
                }
                return $url;
            case 'currentUrl':
            default:
                return preg_replace('/\/[^\/]*\.([Pp][Hh][Pp]|[Hh][Tt][Mm][Ll]|[Cc][Ss][Ss])|(\/*\?.*)/','', $this->userURL);
        }
    }

    /**
     * Возвращает содержимое файла по url. если содержимое заархивировано, разархивирует его
     * @param null $url - URL с которого загружается файл. если он не передан, берется userURL
     * @return bool|string - ошибка в случае неудачи, иначе содержимое файла.
     */
    public function getContent($url = null)
    {
        if(empty($url)) $url = $this->userURL;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // возвращает веб-страницу
        curl_setopt($ch, CURLOPT_HEADER, 0); // не возвращает заголовки
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // переходит по редиректам
        curl_setopt($ch, CURLOPT_ENCODING, ""); // обрабатывает все кодировки
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120); // таймаут соединения
        curl_setopt($ch, CURLOPT_TIMEOUT, 120); // таймаут ответа
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10); // останавливаться после 10-ого редиректа
        if($this->userAgent){
            curl_setopt($ch, CURLOPT_USERAGENT,$this->userAgent);
        }
        $site_content = curl_exec($ch);
        curl_close($ch);
//        $opts = array('http' => array(
//            'method' => "GET",
//            'header' => "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8;\r\n".
//                "Accept-Encoding: gzip,deflate,sdchrn;\r\n"
//        ));
//        $context = stream_context_create($opts);
//        if($result = @file_get_contents($url,false,$context)){
//            foreach($http_response_header as $c => $h){
//                if(stristr($h, 'content-encoding') and stristr($h, 'gzip')){
//                    $result = gzinflate(substr($result,10));
//                }
//            }
//        }
        return $site_content;
    }

    /**
     *  получаем контент из ранее загруженного файла.
     */
    public function getFileContent()
    {
        $this->content = file_get_contents ($this->defaultDir."/index.php");
    }

    /**
     * Возвращает имя файла, по переданному пути к файлу
     * @param $file - путь к файлу
     * @return bool|string - имя файла
     */
    public function getFileName($file)
    {
        $file = str_replace('\\', '/', $file);
        $file = substr($file, (strripos($file, '/') ? strripos($file, '/') + 1 : 0));
        if(preg_match('/[^\w\.\-\@]/', $file, $matches, PREG_OFFSET_CAPTURE)){
            $file = substr($file,0, $matches[0][1]);
        }
        return $file;
    }

    /**
     * Возвращает содержимое файла предварительно загрузив все подключаемые в нем файлы и изменив ссылки на них.
     * @param $content - исходное содержимое файла
     * @param $recursion - массив настроек для поиска содержимого
     *  'img' => [
     *      'patternSearch' => "/(url)\([^\)]*\.([Pp][Nn][Gg]|[Jj][Pp][Gg]|[Ss][Vv][Gg]|[Jj][Pp][Ee][Gg])[^\)]*\)/",
     *      'path' => 'img',
     *      'url' => '../img',
     *  ],
     *  'font' => [
     *      'patternSearch' => "/(url)\([^\)]*\.([Tt][Ii][Ff][Ff]|[Ee][Oo][Tt]|[Ww][Oo][Ff][Ff]|[Tt][Tt][Ff])[^\)]*\)/",
     *      'path' => 'css/font',
     *      'url' => 'font',
     *  ],
     *  'css' => [
     *      'patternSearch' => "/(url)\([^\)]*\.([Cc][Ss][Ss])[^\)]*\)/",
     *      'path' => 'css',
     *      'url' => '../css',
     *      'recursion' => true
     *  ],*
     *
     * @param $attr - относительная| абсолютная ссылка на файл, из которого надо получить содержимое
     * @return bool|mixed - возвращает false, если не удалось сохранить файл. иначе возвращает изменненное содержимое
     */
    public function getRecursionContent($content, $recursion, $attr)
    {
        $userURL = $this->userURL;
        if($attr){
            $this->userURL = $this->parseURL($attr);
        }
        $contentForSearch = str_replace([" "], "", $content);
        foreach ($recursion as $key => $value){
            if(preg_match_all($value['patternSearch'], $contentForSearch, $matches)){
                foreach ($matches[0] as &$match){
                    $match = str_replace([" ", '"', "'"], "", $match);
                    $start = strpos($match, "(");
                    $end = strpos($match, ")");
                    $file = substr($match, $start + 1, $end - $start - 1);
                    if($start === false || $end === false || empty($file)){
                        continue;
                    }
                    $fileName = $this->getFileName($file);
                    if(!$fileName){
                        continue;
                    }
                    $newAttr = $this->parseURL($file);
                    $recContent = $this->getContent($this->parseURL($file));
                    if($recContent === false || empty($recContent)){
                        array_unshift($this->errorsMessages, "Не удалось получить содержимое файла $file по ссылке " . $this->parseURL($file));
                        continue;
                    }
                    if($value['recursion'] === true){
                        $recContent = $this->getRecursionContent($recContent,$recursion,$newAttr);
                    }
                    $dir = $this->defaultDir . '/' . $value['path'];
                    if(!($fileName = $this->saveContentInFile($fileName, $recContent, $dir))){
                        array_unshift($this->errorsMessages, "Не удалось сохранить содержимое файла $fileName в локальную директорию $dir");
                        return false;
                    }
                    $content = str_replace($file, $value['url'] . '/' . $fileName, $content);
                }
            }
        }
        $this->userURL = $userURL;
        return $content;
    }

    /**
     * Загружаем содержимое, по переданной пользователем ссылке userURL
     * работа с содержимомы файла осществляется по указанным настройкам при создание объекта $this->config
     * @return bool - true в случае успеха | false - в случае ошибки
     */
    public function loadContent()
    {
        if(!($this->content = $this->getContent())){
            array_unshift($this->errorsMessages, 'Не удалось загрузить контент. ' . $this->userURL);
            return false;
        }
        foreach ($this->config['download'] as $key => $items){
            $this->elementNumber = 0;
            foreach ($items as $item){
                if(isset($item['patternSearch'])){
                    $elements = $this->returnContentByReg($this->content, $item['patternSearch'], $key);
                }elseif(isset($item['stringSearch'])){
                    $attr = isset($item['stringSearch']['attr']) ? $item['stringSearch']['attr'] : null;
                    $elements = $this->returnContentByTag(
                        $item['stringSearch']['start'],
                        $item['stringSearch']['end'],
                        $this->content,
                        $attr,
                        $key
                    );
                }else{
                    array_unshift($this->errorsMessages, 'Не задан шаблон поиска данных для ' . $key);
                    return false;
                }
                $this->$key = array_merge($this->$key, $elements);
                foreach ($elements as $nameTemp => $value){
                    if($attr = $this->searchAttrValue($value, $item)){
                        $content = $this->getContent($this->parseURL($attr));
                        if($content === false || empty($content)){
                            array_unshift($this->errorsMessages, "Не удалось получить содержимое файла $attr по ссылке " . $this->parseURL($attr));
                            continue;
                        }
                    }else{
                        $content = $this->returnContentWithoutTag($item['stringSearch']['startWithoutTag'],$item['stringSearch']['end'], $value);
                    }
                    $fileName = $attr ?  $this->getFileName($attr) : $nameTemp.".".$key;
                    if(isset($item['recursionContent'])){
                        if(($content = $this->getRecursionContent($content, $item['recursionContent'], $attr)) === false){
                            return false;
                        }
                    }
                    if(!($fileName = $this->saveContentInFile($fileName, $content, $this->defaultDir."/".$key))){
                        array_unshift($this->errorsMessages, "Не удалось сохранить содержимое файла $fileName в локальную директорию " . $this->defaultDir."/".$key);
                        return false;
                    }
                    if(!$this->replaceTemplate($key, $item, $fileName, $nameTemp, $attr)){
                        return false;
                    }
                }
            }
        }
        if(preg_match("/<(meta)[^>]*name\s*=\s*(['\"])viewport\\2[^>]*>/", $this->content)){
            $this->content = preg_replace("/<(meta)[^>]*name\s*=\s*(['\"])viewport\\2[^>]*>/", '<meta name="viewport" content="width=device-width, initial-scale=1">',$this->content);
        }else{
            $this->content = str_replace('</head>','<meta name="viewport" content="width=device-width, initial-scale=1">' . '</head>', $this->content);
        }
        if(!$this->saveContentInFile('index.php', $this->content, $this->defaultDir)){
            array_unshift($this->errorsMessages, "Не удалось сохранить содержимое файла index.php в локальную директорию " . $this->defaultDir);
            return false;
        }
        if($this->searchLinks == true){
            foreach (($this->searchAttrValue($this->content, $this->config['searchLinks'], true)) as $key => $link){
                $this->links[$key] = ['old' => $link, 'href' => ''];
            }
        }
        return true;
    }

    /**
     * находит первый отличный от нуля или false элемент массивы и возвращает его, иначе вернет null
     * @param array $array - массив переменных, может содеражть методы. например '$this->getContent()'
     * @param array $params - содержит параметры для вызываемых методов в $array
     * $array = [
     *      'getContent' => '$this->getContent($url)'
     * ]
     * $params = [
     *      'getContent' => [
     *          'url' => $url
     *      ]
     * ]
     * @return mixed|null
     */
    public function nvl($array = [], $params)
    {
        foreach ($array as $key => $arr){
            if(isset($params[$key])){
                foreach ($params[$key] as $paramName => $param) {
                    $$paramName = $param;
                }
            }
            $result = eval('return '.$arr.';');
            if(!is_null($result) && $result !== false){
                return $result;
            }
        }
        return null;
    }

    /**
     * по относительному URL к файлу генерируем абсолютный и отдаем
     * @param $url - URL к файлу
     * @return mixed|string
     */
    public function parseURL($url)
    {
        $url = str_replace('\\', '/', $url);
        if(preg_match('/([Hh][Tt][Tt][Pp][Ss]?|[Ff][Tt][Pp]|\/{2})/', $url)){
            return preg_replace('(^\/{2})','http://', $url);
        }
        $up = 0;
        if(preg_match('/(\.\.\/)/', $url)){
            $pattern = 'upCurrentUrl';
            while(preg_match('/(\.\.\/)/', $url)){
                $url = preg_replace('/(\.\.\/)/', '', $url, 1);
                $up++;
            }
            $url = "/".$url;
        }elseif(substr($url,0,1) == '/'){
            $pattern = 'onlyDomain';
        }else{
            $pattern = 'currentUrl';
            $url = "/".$url;
        }
        return $this->getBaseUrl($pattern,$up).$url;
    }

    /**
     *  записываем изменненый контент в файл
     */
    public function putFileContent()
    {
        if(!is_integer(file_put_contents($this->defaultDir."/index.php", $this->content))){
            array_unshift($this->errorsMessages, "Не удалось добавить дополнительные библиотеки");
            return false;
        }
        return true;
    }

    /**
     * Удаляем содержимое локальной дерриктории
     * @param null $dir - директория, которую необходимо удалить. если не передано удаляем defaultDir
     */
    public function removeFiles($dir = null)
    {
        if(empty($dir)) $dir = '../tempFile';
        if ($objs = glob($dir."/*")) {
            foreach($objs as $obj) {
                if(filectime($obj) > time() - 60 * 60){
                    continue;
                }
                is_dir($obj) ? $this->removeFiles($obj) : unlink($obj);
            }
        }
        if($dir != '../tempFile')
            rmdir($dir);
    }

    /**
     * заменяем ссылки на новые заданные пользователем.
     */
    public function replaceLinks()
    {
        foreach ($this->links as $key => &$element){
            $this->content = str_replace($element['old'], "{link$key}", $this->content, $count = 1);
            if(empty($element['href'])){
                $element['new'] = $element['old'];
            }else{
                $element['new'] = preg_replace('/[Hh][Rr][Ee][Ff]\=("|\')?([^"|\']*)("|\')?/', 'href="'.$element['href'].'"', $element['old']);
            }
        }
        foreach ($this->links as $key => $element){
            $this->content = str_replace("{link$key}", $element['new'], $this->content);
        }
    }

    /**
     * Заменяем переменные созданные returnContentByTag и returnContentByReg на основее пользовательских шаблонов
     * @param $type - объект, с которым сейчас работает загручик js|css|img
     * @param $item - настройки объекта
     * @param $fileName - название файла, с которым сейчас работает загрузчик
     * @param $key - название переменной которую создали методы returnContentByTag и returnContentByReg
     * @param $attr - заменяемый атрибут, как правило содержимое href src
     * @return bool
     */
    public function replaceTemplate($type, $item, $fileName, $key, $attr)
    {
        if ($attr === false) {
            if(!$item['template']){
                array_unshift($this->errorsMessages, 'Не задан шаблон в config');
                return false;
            }
            $replace = str_replace('{attr}', $type . '/' . $fileName, $item['template']);
        } else {
            $replace = str_replace($attr, $type . '/' . $fileName, $this->{$type}[$key]);
        }
        if($item['comment']){
            $replace = '<!--' . $replace . '-->';
        }
        $this->content = str_replace("{" . $key . "}", $replace , $this->content);
        return true;
    }

    /**
     * Возвращает массив найденных совпадений на основе регулярного выражения.
     * @param $content - контент, в котором осуществляется поиск
     * @param $regExp - регулярное выражения
     * @param $param - тип файла, с которым работает загрузчик
     * @return array - массив найденных совпадений
     */
    public function returnContentByReg(&$content,$regExp,$param)
    {
        $searched = [];
        if (preg_match_all($regExp, $content, $matches)) {
            foreach ($matches[0] as $match) {
                $searched[$param . $this->elementNumber] = $match;
                $content = str_replace ($searched[$param.$this->elementNumber] , "{".$param.$this->elementNumber."}", $content);
                $this->elementNumber++;
            }
        }
        return $searched;
    }

    /**
     * Возвращает массив найденных подстрок между $start и $end
     * @param $start - начало подстроки
     * @param $end - конец подстроки
     * @param $content - контент, в котором осуществляется поиск
     * @param null $attr - название атрибута
     * @param null $param - содержимое атрибута
     * @return array - массив найденных подстрок
     */
    public function returnContentByTag($start, $end, &$content, $attr = null, $param = null)
    {
        $i = 0;
        $searched = [];
        $length = strlen($end);
        while ($startNum = strpos($content,$start, 0)){
            $endNum = strpos($content,$end, $startNum);
            if(!is_string($param)){
                $param = '';
            }
            $search = substr($content, $startNum, $endNum+$length-$startNum);
            if(!is_null($attr) && !preg_match('/'.$attr['attrName'].'\=(\'|\")'.$attr['attrValue'].'(\'|\")/', $search)){
                continue;
            }
            $searched[$param.$i] =  $search;
            $content = str_replace ($searched[$param.$i] , "{".$param.$i."}", $content);
            $i++;
        }
        return $searched;
    }

    /**
     * возращает подстроку переданного контента между $start и $end
     * @param $start - начало подстроки
     * @param $end - конец подстроки
     * @param $content - содержимое
     * @return bool|string
     */
    public function returnContentWithoutTag($start, $end, $content)
    {
        $startNum = strpos($content,$start);
        $endNum = strpos($content,$end, $startNum);
        return substr($content,$startNum + 1, $endNum - $startNum - 1);
    }

    /**
     * Сохраняет содержимое в файл.
     * Если файл уже имеется в директории, то добавляем в начало файла х.
     * @param $fileName - название файла
     * @param $content - содержимое, которое необходимо сохранить
     * @param $dir - директория для сохранения
     * @return bool|string - false, если не удалось создать директорию и сохранить, иначе название файла в
     */
    public function saveContentInFile($fileName, $content, $dir)
    {
        if(file_exists($dir) || mkdir($dir,0777, true)){
            while (file_exists($dir.'/'.$fileName)){
                if(file_get_contents($dir.'/'.$fileName) == $content){
                    return $fileName;                }
                $fileName = 'x' . $fileName;
            }
            $file = fopen($dir.'/'.$fileName,'a');
            fwrite($file, $content);
            fclose($file);
            return $fileName;
        }
        return false;
    }

    /**
     * Возвращает содержимое атрибута переданного тега, иначе false
     * @param $content - конетент, в котором осуществляется поиск
     * @param $item - название атрибута
     * @param $all - возвращать все найденные совпадения
     * @return mixed - string - в случае успеха, false если не найдено
     */
    public function searchAttrValue($content, $item , $all = false)
    {
        if(isset($item['patternAttr'])){
            if(preg_match_all($item['patternAttr']['pattern'], $content, $matches)){
                return $all ? $matches[$item['patternAttr']['matchNumber']] : $matches[$item['patternAttr']['matchNumber']][0];
            }
        }else{
            if(preg_match_all('/\<{1}'.$item['tag'].'[^\>]*'.$item['attr'].'=[\'"]([^\'"]+)[\'"][^\>]*\>/', $content, $matches)){
                return $matches[1][0];
            }
        }
        return false;
    }

    /**
     * добавляем файл в архив
     * @param $zip - ссылка на архив
     * @param $dirLocal - локальная директория файла, добавляемого в архив
     * @param $dirZip - директрия файла в архиве
     */
    public function inArchive(&$zip, $dirLocal, $dirZip)
    {
        if ($objs = glob($dirLocal."/*")) {
            foreach($objs as $obj) {
                $last = substr($obj, (strripos($obj, '/') ? strripos($obj, '/') + 1 : 0));
                if(is_dir($obj)){
                    $this->inArchive($zip, $obj, "$dirZip/$last");
                } else{
                    $zip->addFile($dirLocal."/$last", "$dirZip/$last");
                }
            }
        }
    }

    /**
     * возвращает архив загруженного сайта со всем контентом
     * @return bool - false в сулучае ошибки, иначе отдает клиенту файл
     */
    public function getArchive()
    {
        $zip = new \ZipArchive();
        if ($reuslt = $zip->open($this->defaultDir.'.zip',\ZipArchive::CREATE) === TRUE) {
            $this->inArchive($zip, $this->defaultDir,'');
            $zip->close();
            if (file_exists($this->defaultDir.'.zip')) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="'.basename($this->defaultDir.'.zip').'"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($this->defaultDir.'.zip'));
                readfile($this->defaultDir.'.zip');
                exit;
            }
            $this->errorsMessages[] = 'Не удалось отправить '.$this->defaultDir.'.zip попробуйте повторить позже.';
            return false;
        } else {
           return false;
        }
    }
}