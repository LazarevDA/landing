<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 04.09.2017
 * Time: 15:43
 */

namespace app\models;

use yii\base\Model;

class FTP extends Model
{
    public $connection;
    public $errorsMessages = [];
    public $ftpDir;
    private $ftp;
    private $jsHeadPath = '/varyhelp.org/libs/js/head';
    private $jsBodyPath = '/varyhelp.org/libs/js/body';
    private $faviconPath = '/varyhelp.org/libs/favicon';
    private $phpPath = '/varyhelp.org/libs/php';
    private $userFile = [
        'favicon' => [],
        'jsHead' => [],
        'jsBody' => [],
        'php' => [],
    ];

    public function __construct(array $config = [])
    {
        $this->connection = true;
        parent::__construct($config);
        if(!($this->ftp = ftp_connect(FTP_HOST))){
            array_unshift($this->errorsMessages, 'Не удалось подключиться к удаленному серверу');
            $this->connection = false;
        }
        if (!@ftp_login($this->ftp, FTP_USER, FTP_PASS)) {
            array_unshift($this->errorsMessages, "Не удалось авторизирвоаться под именем ".FTP_USER);
            $this->connection = false;
        }

    }

    /**
     * Переходим в указанную директорию, если ее нет - создаем
     * @param $ftp - подключение к ftp через ftp_connect()
     * @param $dir - директория в которую нужно перейти
     * @return mixed возвращает директорию при успешном создании
     */
    public function MKDir(&$ftp, $dir)
    {
        @ftp_chdir($ftp, '/');
        $parts = explode('/',$dir);
        $parts[0] = '/';
        foreach($parts as $part){
            if(!@ftp_chdir($ftp, $part)){
                ftp_mkdir($ftp, $part);
                ftp_chdir($ftp, $part);
            }
        }
        return $dir;
    }

    public function scanDirectory()
    {
        foreach ($this->userFile as $name => $item) {
            if ($objs = ftp_nlist($this->ftp, $this->{$name."Path"})) {
                foreach($objs as $obj) {
                    $this->userFile[$name][$obj] = substr($obj,strripos($obj, '/') + 1);
                }
            }else{
                array_unshift($this->errorsMessages, 'Не удалось получить список файлов из '.$this->{$name."Path"});
                return false;
            }
        }
        return true;
    }

    public function getuserFile()
    {
        return $this->userFile;
    }

    /**
     * загружает файлы на FTP из локальной диретории
     * @param $localDir - локальная директория
     * @param $ftpDir - директория на ftp
     * @return bool
     */
    public function uploadOnFTP($localDir, $ftpDir)
    {
        if($objs = scandir($localDir)){
            foreach ($objs as $obj){
                if($obj == '.' || $obj == '..'){
                    continue;
                }
                $localName = $localDir.'/'.$obj;
                $ftpName = $ftpDir.'/'.$obj;
                if(is_dir($localName)){
                    $this->uploadOnFTP($localName, $ftpName);
                }else{
                    $dir = $this->MKDir($this->ftp, $ftpDir);
                    if($dir && $dir === $ftpDir){
                        if(false === $saveResult = @ftp_put($this->ftp, $ftpName, $localName, FTP_BINARY)){
                            array_unshift($this->errorsMessages, "Не удалось сохранить файл $localName на ftp $ftpName");
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

}