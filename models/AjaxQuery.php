<?php
/**
 * Created by PhpStorm.
 * User: Денис
 * Date: 11.08.2017
 * Time: 21:34
 */

namespace app\models;

use Yii;

class AjaxQuery extends Main
{
    private $aRequestParams;
    public function viewAjaxQuery($AjaxQueryController = null)
    {
        if (!Yii::$app->request->isAjax) {
            return json_encode(['result'=> false, 'data' => 'Ошибка запроса 666. Данные не сохранены.']);
        }
        $this->aRequestParams = Yii::$app->request->getBodyParams();
        if(!method_exists($this,$this->aRequestParams['action'])){
            return json_encode(['result'=> false, 'data' => "Ошибка запроса. Метод ".$this->aRequestParams['action']." не найден."]);
        }
        return json_encode($this->{$this->aRequestParams['action']}($AjaxQueryController));
    }

    private function getSettingContent($AjaxQueryController)
    {
        $modelFTP = new FTP();
        if(!$modelFTP->connection){
            return ['result' => false, 'content' => 'Не удалось подключиться к удаленному серверу'];
        }
        if($modelFTP->scanDirectory()){
            return  ['result' => true,
                'content' => $AjaxQueryController->renderPartial(
                    'setting-content', [
                    'modelFTP' => $modelFTP,
                ])];
        }else{
            foreach ($modelFTP->errorsMessages as $message){
                $messages[] = ['message' => $message, 'type' => 'warning'];
            }
            return  ['result' => false,
                'content' => $AjaxQueryController->renderPartial(
                    '../layouts/messages', [
                    'messages' => $messages,
                ])];
        }

    }

}