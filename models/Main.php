<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 04.09.2017
 * Time: 15:43
 */

namespace app\models;


use yii\db\ActiveRecord;

class Main extends ActiveRecord
{

    private $tpl_date = [
        '01' => 'January',
        '02' => 'February',
        '03' => 'March',
        '04' => 'April',
        '05' => 'May',
        '06' => 'June',
        '07' => 'July',
        '08' => 'August',
        '09' => 'September',
        '10' => 'October',
        '11' => 'November',
        '12' => 'December',
    ];

    public function afterFind($withTime = false)
    {
        parent::afterFind();
        $this->date_created = date(($withTime ? 'd.m.Y H:i:s' : 'd.m.Y'), $this->date_created);
    }
    public function beforeSave($insert)
    {
        if(!is_integer($this->date_created) || strlen($this->date_created) != 10){
            if(!preg_match('/(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}/',$this->date_created)){
                $this->addError('date_created', 'Некорректная дата');
                return false;
            }
            $this->date_created = $this->parseDateToTime($this->date_created, true);
        }
        return parent::beforeSave($insert);
    }

    public function merge_array($arrays, $names = null)
    {
        if(
            !is_array($arrays)
            || ($names !== null && !is_array($names))
            || (is_array($names) && count($arrays) !== count($names))
        ){
            return false;
        }
        if($names === null){
            for ($i = 0; $i < count($arrays); $i++){
                $names[] = $i;
            }
        }
        $newArray = [];
        foreach ($names as $i => $name){
            foreach ($arrays[$i] as $key => $value){
                $newArray[$key][$name] = $value;
            }
        }
        return $newArray;
    }

    public function parseDateToTime($date, $flag = false)
    {
        $aDate = explode('-', $date);
        $newDate = $aDate[2]. ' ' . $this->tpl_date[$aDate[1]]. ' ' . $aDate[0] . ($flag ? ' 00:00:00' : ' 23:59:59');
        return strtotime($newDate);
    }

    public function getHttpContent($url, $method, $params)
    {
        $opts = array('http' => array(
            'method' => "$method",
            'header' => "Accept:text/html,application/xhtml+xml,application/xml;\r\n"
        ));
        $context = stream_context_create($opts);
        $sParams = '';
        foreach($params as $key => $value){
            if(empty($sParams)){
                $sParams .= "?$key=$value";
            }else{
                $sParams .= "&$key=$value";
            }
        }
        $sContent = file_get_contents($url.$sParams,false,$context);
        return $sContent;
    }

    public function parseFormatDate($sDate)
    {
        $dates = explode('_', $sDate);
        $newDate = '';
        foreach ($dates as $date){
            $aDate = explode('-', $date);
            if(empty($newDate)){
                $newDate .= $aDate[2] . "." . $aDate[1] . "." . substr($aDate[0],2);
            }else{
                $newDate .= "-" . $aDate[2] . "." . $aDate[1] . "." . substr($aDate[0],2);
            }
        }
        return $newDate;
    }

    /**
     * @param array $array
     * @param string $attr
     * @return mixed|null
     */
    public function nvl($array = [],$attr)
    {
        foreach ($array as $arr){
            $result = eval('return '.$arr);
            if(!is_null($result) && $result !== false){
                return $result;
            }
        }
        return null;
    }
}