<?php
require_once(__DIR__ . '/../config/config.php');
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host='.DB_HOST.';dbname='.DB_NAME,
    'username' => DB_USERNAME,
    'password' => DB_PASSWORD,
    'charset' => DB_CHARSET,
];
