<?php
/**
 * Created by PhpStorm.
 * User: Денис
 * Date: 11.08.2017
 * Time: 21:33
 */

namespace app\controllers;

use yii\web\Controller;
use app\models\AjaxQuery;

class AjaxQueryController extends Controller
{
    public function actionGetAjax()
    {
        $ajaxQuery = new AjaxQuery();
        echo $ajaxQuery->viewAjaxQuery($this);
    }
}