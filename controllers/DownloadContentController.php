<?php

namespace app\controllers;

use app\models\DownloadContent;
use app\models\FTP;
use Yii;
use yii\web\Controller;

class DownloadContentController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $start = time();
        $messages = [];
        if(Yii::$app->request->isPost){
            $post = Yii::$app->request->post();
            $defaultDir = !empty($post['DownloadContent']['defaultDir']) ?
                $post['DownloadContent']['defaultDir'] :
                "../tempFile/site_".date("Y-m-d_H-i-s", time());
        }else{
            $defaultDir = null;
        }
        $modelLoader = new DownloadContent([
            'config' => [
                'download' => [
                    'css' => [
                        [
                            'stringSearch' => [
                                'start' => '<link',
                                'end' => '>',
                                'attr' => [
                                    'attrName' => 'rel',
                                    'attrValue' => 'stylesheet',
                                ],
                            ],
                            'patternSearch' => "/\<[Ll][Ii][Nn][Kk][^\>]*[Rr][Ee][Ll]\=(\"|')([Ss][Tt][Yy][Ll][Ee][Ss][Hh][Ee][Ee][Tt])(\"|')[^\>]*>/",
                            'attr' => 'href',
                            'tag' => '[Ll][Ii][Nn][Kk]',
                            'recursionContent' => [
                                'img' => [
                                    'patternSearch' => "/(url)\([^\)]*\.([Pp][Nn][Gg]|[Jj][Pp][Gg]|[Ss][Vv][Gg]|[Jj][Pp][Ee][Gg])[^\)]*\)/",
                                    'path' => 'img',
                                    'url' => '../img',
                                ],
                                'font' => [
                                    'patternSearch' => "/(url)\([^\)]*\.([Tt][Ii][Ff][Ff]|[Ee][Oo][Tt]|[Ww][Oo][Ff][Ff]|[Tt][Tt][Ff])[^\)]*\)/",
                                    'path' => 'css/font',
                                    'url' => 'font',
                                ],
                                'css' => [
                                    'patternSearch' => "/(url)\([^\)]*\.([Cc][Ss][Ss])[^\)]*\)/",
                                    'path' => 'css',
                                    'url' => '../css',
                                    'recursion' => true
                                ],
                            ]
                        ]
                    ],
                    'img' => [
                        [
                            'patternSearch' => '/\<[Aa][^\>]*[Hh][Rr][Ee][Ff]\=("|\')[^"|\']*.([Pp][Nn][Gg]|[Jj][Pp][Gg]|[Ss][Vv][Gg]|[Jj][Pp][Ee][Gg])("|\')[^\>]*\>/',
                            'attr' => 'href',
                            'tag' => '[Aa]',
                        ],
                        [
                            'patternSearch' => '/(url)\([^\)]*\.([Pp][Nn][Gg]|[Jj][Pp][Gg]|[Ss][Vv][Gg]|[Jj][Pp][Ee][Gg])[^\)]*\)/',
                            'patternAttr' => [
                                'pattern' => '/(\(["\']?)([^\)]*)(["\']?\))/',
                                'matchNumber' => 2,
                            ]
                        ],
                        [
                            'stringSearch' => [
                                'start' => '<img',
                                'end' => '>',
                            ],
                            'patternSearch' => '/(\<[Ii][Mm][Gg])[^\>]*[Ss][Rr][Cc]\=[\'"][^\>]+[\'"][^\>]*\>/',
                            'attr' => 'src',
                            'tag' => '[Ii][Mm][Gg]',
                        ],
                    ],
                    'js' => [
                        [
                            'stringSearch' => [
                                'startWithoutTag' => '>',
                                'start' => '<script',
                                'end' => '</script>',
                            ],
                            'attr' => 'src',
                            'tag' => '[Ss][Cc][Rr][Ii][Pp][Tt]',
                            'template' => '<script src="{attr}" type="text/javascript"></script>',
                            'comment' => true,
                        ]
                    ],
                ],
                'searchLinks' => [
                    'patternAttr' => [
                        'pattern' => '/\<[Aa][^\>]*[Hh][Rr][Ee][Ff]\=("|\')?([^"|\']*)("|\')?[^\>]*\>/',
                        'matchNumber' => 0,
                        ]
                ],
            ],
            'defaultDir' => $defaultDir
        ]);
        $modelLoader->removeFiles();
        if(!$modelLoader->load(Yii::$app->request->post())){
            return $this->render('index',[
                'modelLoader' => $modelLoader
            ]);
        }
        if(isset($post['download'])){
            if($modelLoader->downloadResult = $modelLoader->loadContent()){
                $messages[] = [
                    'message' =>  'Страница загружена'.(empty($modelLoader->errorsMessages) ? '.' : ', но возникли следующие ошибки.'),
                    'type' => 'success'
                ];
            }else{
                $messages[] = ['message' => 'Что-то не получилось! :\'(', 'type' => 'danger'];
            }
        }
        if(isset($post['upload'])){
            $modelLoader->getFileContent();
            if(!empty($modelLoader->addUserContent)){
                $modelLoader->addUserContent();
            }
            if(!empty($modelLoader->searchLinks)){
                $modelLoader->replaceLinks();
            }
            if(!$modelLoader->putFileContent()){
                goto lastRender;
            }
            if($modelLoader->uploadType === 'zip'){
                $modelLoader->getArchive();
            }elseif($modelLoader->uploadType === 'ftp'){
                if(Yii::$app->user->isGuest){
                    $messages[] = [
                        'message' =>  'Загружать на FTP могут только авторизированные пользователи',
                        'type' => 'warning'
                    ];
                    goto lastRender;
                }
                $modelFTP = new FTP();
                if($modelFTP->connection){
                    if($modelFTP->uploadOnFTP($modelLoader->defaultDir, $modelLoader->ftpDir)){
                        $messages[] = [
                            'message' =>  'Страница загружена на ftp (<a href="http://'.substr($modelLoader->ftpDir,1).'" target="_blank">'.substr($modelLoader->ftpDir,1).'</a> )',
                            'type' => 'success'];
                    }
                }
                if(!empty($modelFTP->errorsMessages)){
                    foreach ($modelFTP->errorsMessages as $error) {
                        $messages[] = ['message' => $error, 'type' => 'danger'];
                    }
                }
            }
        }

        lastRender:
        if(!empty($modelLoader->errorsMessages)){
            foreach ($modelLoader->errorsMessages as $error) {
                $messages[] = ['message' => $error, 'type' => 'danger'];
            }
        }
        $end = time();
        array_unshift($messages, [
            'message' => 'Время работы скрипта: '. date('Y.m.d H:i:s', $start) . ' - ' . date('H:i:s', $end) . ' (' .($end - $start) . ' сек)',
            'type' => 'info'
        ]);
        return $this->render('index',[
            'modelLoader' => $modelLoader,
            'messages' => $messages,
        ]);
    }


}
