<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'OneKey.mobi';
?>
<div class="parser-landing-index">
    <?=$this->render('../layouts/messages', ['messages' => $messages])?>
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($modelLoader, 'userURL')->textInput() ?>

    <?= $form->field($modelLoader, 'userAgent')->textInput() ?>

    <?= $form->field($modelLoader, 'defaultDir')->hiddenInput(['value' => $modelLoader->defaultDir])->label(false) ?>

    <?= $form->field($modelLoader, 'downloadResult')->hiddenInput(['value' => (string)$modelLoader->downloadResult])->label(false) ?>

    <?= $form->field($modelLoader, 'searchLinks')->checkbox();?>

    <?php if($modelLoader->downloadResult == true){ ?>
        <?= $this->render('form-after-download.php', ['form' => $form, 'modelLoader' => $modelLoader]) ?>
    <?php }else{ ?>
    <div class="form-group">
        <?= Html::submitButton("Скачать", ["class" => "btn btn-success", "name" => "download"])?>
    </div>
    <?php } ?>
    <?php ActiveForm::end(); ?>
</div>
