<?php

use yii\helpers\Html;
?>
<?php if($modelLoader->searchLinks == true) {?>
<div class="container-links">
    <div class="panel panel-default">
        <div class="panel-heading">
            <button type="button" class="btn btn-default spoiler-trigger" data-toggle="collapse">Ссылки</button>
        </div>
        <div class="panel-collapse collapse out">
            <div class="panel-body">
                <?php
                if(empty($modelLoader->links)){
                    $str = '<p>Ссылки не найдены</p>';
                }else{
                    $str = '';
                    foreach ($modelLoader->links as $ke => $link){
                        $str .= '
                <div class="row delete-group">
                    <div class="col-sm-5 text-right">
                        <input type="text" class="form-control unique value_base required" value="'.Html::encode($link['old']).'" name="DownloadContent[links]['.$ke.'][old]" readonly>
                    </div>
                    <div class="col-sm-2 text-center">
                        =>
                    </div>
                    <div class="col-sm-5 text-left">
                        <input type="text" class="form-control unique value_file required" value="'.Html::encode($link['href']).'" name="DownloadContent[links]['.$ke.'][href]">
                    </div>
                </div>
                        ';
                    }
                }
                echo $str;
                ?>
            </div>
        </div>
    </div>
</div>
<?php }?>
<div class="container-settings">
    <div class="panel panel-default">
        <div class="panel-heading">
            <button type="button" id="settings" class="btn btn-default spoiler-trigger" data-toggle="collapse">Дополнительные настройки</button>
        </div>
        <div class="panel-collapse collapse out">
            <div class="panel-body" id="settings-content">

            </div>
        </div>
    </div>
</div>
<div class="row">
<?= $form->field($modelLoader, 'uploadType', ['options' => ['class' => 'col-md-2 form-group']])
    ->radioList([
            'ftp' => 'На FTP',
            'zip' => 'Zip',
        ],
        [
            'encode' => false,
            'unselect' => false,
            'item' => function($index, $label, $name, $checked, $value){
                 return '<div class="radio"><label style="display: block;"><input type="radio" class="uploadType" name="'.$name.'" value="'.$value.'" '.($checked ? 'checked="checked"' : "").'>'.$label.'</label></div>';
            }

    ]); ?>
<?=  $form->field($modelLoader,'ftpDir', ['options' => ['class' => 'col-md-10 form-group']])->textInput([
    'placeholder' => 'Введите директорию для сохранения на сервере',
    'class' => 'form-control ftpDir',
    'disabled' => $modelLoader->uploadType == 'ftp' ? false : true,
])
?>
</div>



<div class="form-group">
    <?= Html::submitButton('Выгрузить', ['class' => 'btn btn-success', 'name' => 'upload']) ?>
</div>