<?php

use yii\helpers\Html;
?>

<div class="form-group">
    <?= Html::button('Обновить', ['class' => 'btn btn-info', 'id' => 'updateButton']) ?>
</div>
<div class="form-group">
    <label for="favicon">Выберите иконку</label>
    <?=Html::dropDownList(
            'DownloadContent[addUserContent][favicon][]',
            null,
            $modelFTP->userFile['favicon'],
            [
                'class' => 'form-control',
                'id' => 'favicon'
            ])?>
</div>
<div class="form-group">
    <label for="jsHead">JS подключаемые в HEAD</label>
    <div class="checkbox">
        <?=Html::checkboxList(
            'DownloadContent[addUserContent][jsHead]',
            null,
            $modelFTP->userFile['jsHead'],
            [
                'tag' =>false,
                'item' => function($index, $label, $name, $checked, $value){
                    return '<div class="checkbox"><label><input type="checkbox" name="'.$name.'" value="'.$value.'" '.($checked ? 'checked="checked"' : ''.'> '.$label).'</label></div>';
                }
            ]
        )?>
    </div>
</div>
<div class="form-group">
    <label for="jsBody">JS подключаемые в BODY</label>
    <div class="checkbox">
        <?=Html::checkboxList(
            'DownloadContent[addUserContent][jsBody]',
            null,
            $modelFTP->userFile['jsBody'],
            [
                'tag' =>false,
                'item' => function($index, $label, $name, $checked, $value){
                    return '<div class="checkbox"><label><input type="checkbox" name="'.$name.'" value="'.$value.'" '.($checked ? 'checked="checked"' : '').'> '.$label.'</label></div>';
                }
            ]
        )?>
    </div>
</div>
<div class="form-group">
    <label for="php">Подключаемые php скрипты</label>
    <div class="checkbox">
        <?=Html::checkboxList(
            'DownloadContent[addUserContent][php]',
            null,
            $modelFTP->userFile['php'],
            [
                'tag' =>false,
                'item' => function($index, $label, $name, $checked, $value){
                    return '<div class="checkbox"><label><input type="checkbox" name="'.$name.'" value="'.$value.'" '.($checked ? 'checked="checked"' : '').'> '.$label.'</label></div>';
                }
            ]
        )?>
    </div>
</div>
