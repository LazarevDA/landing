<?php

if (isset($messages)){
    $alerts = '';
    foreach ($messages as $message){
        $alerts .= '
<div class="alert alert-'.$message['type'].' alert-dismissible" role="alert" style="word-wrap: break-word;">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  '.$message['message'].'
</div>';
    }
    echo $alerts;
}
