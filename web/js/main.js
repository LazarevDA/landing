var setting_content = false;
$(document).ready(function () {
    $(document).on('click', '#settings', addSettingsContent);
    $(".spoiler-trigger").click(function() {
        $(this).parent().next().collapse('toggle');
    });
    $(this).on('change','.uploadType', function () {
        $('.ftpDir').prop('disabled', !($(this).val() === 'ftp')).blur();
    });
    $(document).on('click', '#updateButton', function(){setting_content=false;addSettingsContent();});
});

var addSettingsContent = function () {
    if(setting_content){
        return true;
    }
    $('#settings-content').html('Загрузка контента...');
    $.ajax({
        url: '../ajax-query/get-ajax',
        type: 'POST',
        dataType: 'JSON',
        data: {
            action: 'getSettingContent'
        },
        success: function (data) {
            $('#settings-content').html(data.content);
            setting_content = data.result;
        },
        error: function (data) {
            console.log(data);
        }
    });
};